﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Restaurante
{
    public partial class Form1 : Form
    {
        Restaurante restaurante;

        public Form1()
        {
            InitializeComponent();
            restaurante = new Restaurante(new[] {4, 4, 2, 6});
            /*
            mesas = new Mesa[4];

            for (int i = 0; i < 4; i++)
                mesas[i] = new Mesa();
             */
        }

        public int numeroMesa (Button sender)
        {
            Button boton = (Button)sender;

            for (int i = 1; i <= panelBotones.Controls.Count / 3; i++)
            {
                if (boton.Name.IndexOf((i + 1).ToString()) != -1)
                {
                    return i;
                }
            }

            return 0;
        }

        // Eventos
        private void btnOcuM_Click(object sender, EventArgs e)
        {
            int num = numeroMesa ((Button) sender);
            
        }

        private void btnResM_Click(object sender, EventArgs e)
        {
        }

        private void btnDesM_Click(object sender, EventArgs e)
        {
        }
    }
}
