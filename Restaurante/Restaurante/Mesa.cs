﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurante
{
    class Mesa
    {
        public enum EstadoMesa { Libre, Reservada, Ocupada };

        private int numOcupantesMAX;
        private int numOcupantes;
        private EstadoMesa estado;
        private string horaOcupacion;

        public Mesa ()
        {
            numOcupantesMAX = 4;
            numOcupantes = 0;
            estado = EstadoMesa.Libre;
            horaOcupacion = "";
        }
        public Mesa (int numOcuMAX, int numOcu, EstadoMesa estado)
        {
            numOcupantesMAX = numOcuMAX;
            numOcupantes = numOcu;
            this.estado = estado;
            horaOcupacion = "";
        }
        public Mesa(int numOcuMAX)
        {
            numOcupantesMAX = numOcuMAX;
            numOcupantes = 0;
            estado = EstadoMesa.Libre;
            horaOcupacion = "";
        }

        public int NumOcupantesMAX { get { return numOcupantesMAX; } set { numOcupantesMAX = value; } }
        public int NumOcupantes { get { return numOcupantes; } set { numOcupantes = value; } }
        public EstadoMesa Estado { get { return estado; } set { estado = value; } }
        public string HoraOcupacion { get { return horaOcupacion; } }
        
        // 
        public void ocupar (int numOcu)
        {
            if (numOcu <= numOcupantesMAX && estado == EstadoMesa.Libre)
            {
                numOcupantes = numOcu;
                estado = EstadoMesa.Ocupada;
                horaOcupacion = DateTime.Now.Hour + ":" + DateTime.Now.Minute;
            }
        }

        public void cobrar()
        {
            if (estado == EstadoMesa.Ocupada)
            {
                numOcupantes = 0;
                estado = EstadoMesa.Libre;
                horaOcupacion = "";
            }
        }

        public void reservar ()
        {
            if (estado == EstadoMesa.Libre)
                estado = EstadoMesa.Reservada;
        }
    }
}
