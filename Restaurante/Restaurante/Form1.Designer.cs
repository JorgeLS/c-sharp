﻿namespace Restaurante
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Mesa1 = new System.Windows.Forms.Label();
            this.lblMesa1 = new System.Windows.Forms.Label();
            this.panelMesas = new System.Windows.Forms.Panel();
            this.lblMesa2 = new System.Windows.Forms.Label();
            this.lblMesa3 = new System.Windows.Forms.Label();
            this.lblMesa4 = new System.Windows.Forms.Label();
            this.Mesa2 = new System.Windows.Forms.Label();
            this.Mesa3 = new System.Windows.Forms.Label();
            this.Mesa4 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btnOcuM1 = new System.Windows.Forms.Button();
            this.btnResM1 = new System.Windows.Forms.Button();
            this.btnDesM1 = new System.Windows.Forms.Button();
            this.panelBotones = new System.Windows.Forms.Panel();
            this.btnDesM4 = new System.Windows.Forms.Button();
            this.btnDesM3 = new System.Windows.Forms.Button();
            this.btnDesM2 = new System.Windows.Forms.Button();
            this.btnResM4 = new System.Windows.Forms.Button();
            this.btnOcuM4 = new System.Windows.Forms.Button();
            this.btnResM3 = new System.Windows.Forms.Button();
            this.btnOcuM3 = new System.Windows.Forms.Button();
            this.btnResM2 = new System.Windows.Forms.Button();
            this.btnOcuM2 = new System.Windows.Forms.Button();
            this.panelMesas.SuspendLayout();
            this.panelBotones.SuspendLayout();
            this.SuspendLayout();
            // 
            // Mesa1
            // 
            this.Mesa1.AutoSize = true;
            this.Mesa1.BackColor = System.Drawing.Color.White;
            this.Mesa1.Location = new System.Drawing.Point(20, 67);
            this.Mesa1.Margin = new System.Windows.Forms.Padding(20, 0, 3, 0);
            this.Mesa1.MinimumSize = new System.Drawing.Size(50, 50);
            this.Mesa1.Name = "Mesa1";
            this.Mesa1.Size = new System.Drawing.Size(50, 50);
            this.Mesa1.TabIndex = 0;
            this.Mesa1.Text = "0/4";
            this.Mesa1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMesa1
            // 
            this.lblMesa1.AutoSize = true;
            this.lblMesa1.Location = new System.Drawing.Point(7, 16);
            this.lblMesa1.Name = "lblMesa1";
            this.lblMesa1.Size = new System.Drawing.Size(63, 18);
            this.lblMesa1.TabIndex = 1;
            this.lblMesa1.Text = "Mesa 1";
            // 
            // panelMesas
            // 
            this.panelMesas.Controls.Add(this.lblMesa1);
            this.panelMesas.Controls.Add(this.lblMesa2);
            this.panelMesas.Controls.Add(this.lblMesa3);
            this.panelMesas.Controls.Add(this.lblMesa4);
            this.panelMesas.Controls.Add(this.Mesa1);
            this.panelMesas.Controls.Add(this.Mesa2);
            this.panelMesas.Controls.Add(this.Mesa3);
            this.panelMesas.Controls.Add(this.Mesa4);
            this.panelMesas.Location = new System.Drawing.Point(112, 40);
            this.panelMesas.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.panelMesas.Name = "panelMesas";
            this.panelMesas.Size = new System.Drawing.Size(566, 138);
            this.panelMesas.TabIndex = 2;
            // 
            // lblMesa2
            // 
            this.lblMesa2.AutoSize = true;
            this.lblMesa2.Location = new System.Drawing.Point(160, 16);
            this.lblMesa2.Name = "lblMesa2";
            this.lblMesa2.Size = new System.Drawing.Size(63, 18);
            this.lblMesa2.TabIndex = 1;
            this.lblMesa2.Text = "Mesa 2";
            // 
            // lblMesa3
            // 
            this.lblMesa3.AutoSize = true;
            this.lblMesa3.Location = new System.Drawing.Point(313, 16);
            this.lblMesa3.Name = "lblMesa3";
            this.lblMesa3.Size = new System.Drawing.Size(63, 18);
            this.lblMesa3.TabIndex = 1;
            this.lblMesa3.Text = "Mesa 3";
            // 
            // lblMesa4
            // 
            this.lblMesa4.AutoSize = true;
            this.lblMesa4.Location = new System.Drawing.Point(466, 16);
            this.lblMesa4.Name = "lblMesa4";
            this.lblMesa4.Size = new System.Drawing.Size(63, 18);
            this.lblMesa4.TabIndex = 1;
            this.lblMesa4.Text = "Mesa 4";
            // 
            // Mesa2
            // 
            this.Mesa2.AutoSize = true;
            this.Mesa2.BackColor = System.Drawing.Color.White;
            this.Mesa2.Location = new System.Drawing.Point(173, 67);
            this.Mesa2.Margin = new System.Windows.Forms.Padding(100, 0, 3, 0);
            this.Mesa2.MinimumSize = new System.Drawing.Size(50, 50);
            this.Mesa2.Name = "Mesa2";
            this.Mesa2.Size = new System.Drawing.Size(50, 50);
            this.Mesa2.TabIndex = 0;
            this.Mesa2.Text = "0/4";
            this.Mesa2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Mesa3
            // 
            this.Mesa3.AutoSize = true;
            this.Mesa3.BackColor = System.Drawing.Color.White;
            this.Mesa3.Location = new System.Drawing.Point(326, 67);
            this.Mesa3.Margin = new System.Windows.Forms.Padding(100, 0, 3, 0);
            this.Mesa3.MinimumSize = new System.Drawing.Size(50, 50);
            this.Mesa3.Name = "Mesa3";
            this.Mesa3.Size = new System.Drawing.Size(50, 50);
            this.Mesa3.TabIndex = 0;
            this.Mesa3.Text = "0/2";
            this.Mesa3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Mesa4
            // 
            this.Mesa4.AutoSize = true;
            this.Mesa4.BackColor = System.Drawing.Color.White;
            this.Mesa4.Location = new System.Drawing.Point(479, 67);
            this.Mesa4.Margin = new System.Windows.Forms.Padding(100, 0, 20, 0);
            this.Mesa4.MinimumSize = new System.Drawing.Size(50, 50);
            this.Mesa4.Name = "Mesa4";
            this.Mesa4.Size = new System.Drawing.Size(50, 50);
            this.Mesa4.TabIndex = 0;
            this.Mesa4.Text = "0/6";
            this.Mesa4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.Color.White;
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Items.AddRange(new object[] {
            "Plazas Libres:",
            "Plazas Ocupadas:",
            "Caja Actual:",
            "Cantidad pendiente de pagar: ",
            "Mesa 1:",
            "Mesa 2:",
            "Mesa 3:",
            "Mesa 4:"});
            this.listBox1.Location = new System.Drawing.Point(112, 379);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(288, 164);
            this.listBox1.TabIndex = 1;
            // 
            // btnOcuM1
            // 
            this.btnOcuM1.Location = new System.Drawing.Point(3, 11);
            this.btnOcuM1.Name = "btnOcuM1";
            this.btnOcuM1.Size = new System.Drawing.Size(100, 35);
            this.btnOcuM1.TabIndex = 3;
            this.btnOcuM1.Text = "Ocupar";
            this.btnOcuM1.UseVisualStyleBackColor = true;
            this.btnOcuM1.Click += new System.EventHandler(this.btnOcuM_Click);
            // 
            // btnResM1
            // 
            this.btnResM1.Location = new System.Drawing.Point(3, 52);
            this.btnResM1.Name = "btnResM1";
            this.btnResM1.Size = new System.Drawing.Size(100, 35);
            this.btnResM1.TabIndex = 3;
            this.btnResM1.Text = "Reservar";
            this.btnResM1.UseVisualStyleBackColor = true;
            this.btnResM1.Click += new System.EventHandler(this.btnResM_Click);
            // 
            // btnDesM1
            // 
            this.btnDesM1.Location = new System.Drawing.Point(4, 93);
            this.btnDesM1.Name = "btnDesM1";
            this.btnDesM1.Size = new System.Drawing.Size(100, 35);
            this.btnDesM1.TabIndex = 3;
            this.btnDesM1.Text = "Desocupar";
            this.btnDesM1.UseVisualStyleBackColor = true;
            this.btnDesM1.Click += new System.EventHandler(this.btnDesM_Click);
            // 
            // panelBotones
            // 
            this.panelBotones.Controls.Add(this.btnDesM4);
            this.panelBotones.Controls.Add(this.btnDesM3);
            this.panelBotones.Controls.Add(this.btnDesM2);
            this.panelBotones.Controls.Add(this.btnDesM1);
            this.panelBotones.Controls.Add(this.btnResM4);
            this.panelBotones.Controls.Add(this.btnOcuM4);
            this.panelBotones.Controls.Add(this.btnResM3);
            this.panelBotones.Controls.Add(this.btnOcuM3);
            this.panelBotones.Controls.Add(this.btnResM2);
            this.panelBotones.Controls.Add(this.btnOcuM2);
            this.panelBotones.Controls.Add(this.btnResM1);
            this.panelBotones.Controls.Add(this.btnOcuM1);
            this.panelBotones.Location = new System.Drawing.Point(112, 192);
            this.panelBotones.Name = "panelBotones";
            this.panelBotones.Size = new System.Drawing.Size(566, 138);
            this.panelBotones.TabIndex = 4;
            // 
            // btnDesM4
            // 
            this.btnDesM4.Location = new System.Drawing.Point(454, 93);
            this.btnDesM4.Name = "btnDesM4";
            this.btnDesM4.Size = new System.Drawing.Size(100, 35);
            this.btnDesM4.TabIndex = 3;
            this.btnDesM4.Text = "Desocupar";
            this.btnDesM4.UseVisualStyleBackColor = true;
            this.btnDesM4.Click += new System.EventHandler(this.btnDesM_Click);
            // 
            // btnDesM3
            // 
            this.btnDesM3.Location = new System.Drawing.Point(308, 93);
            this.btnDesM3.Name = "btnDesM3";
            this.btnDesM3.Size = new System.Drawing.Size(100, 35);
            this.btnDesM3.TabIndex = 3;
            this.btnDesM3.Text = "Desocupar";
            this.btnDesM3.UseVisualStyleBackColor = true;
            this.btnDesM3.Click += new System.EventHandler(this.btnDesM_Click);
            // 
            // btnDesM2
            // 
            this.btnDesM2.Location = new System.Drawing.Point(154, 93);
            this.btnDesM2.Name = "btnDesM2";
            this.btnDesM2.Size = new System.Drawing.Size(99, 35);
            this.btnDesM2.TabIndex = 3;
            this.btnDesM2.Text = "Desocupar";
            this.btnDesM2.UseVisualStyleBackColor = true;
            this.btnDesM2.Click += new System.EventHandler(this.btnDesM_Click);
            // 
            // btnResM4
            // 
            this.btnResM4.Location = new System.Drawing.Point(453, 52);
            this.btnResM4.Name = "btnResM4";
            this.btnResM4.Size = new System.Drawing.Size(100, 35);
            this.btnResM4.TabIndex = 3;
            this.btnResM4.Text = "Reservar";
            this.btnResM4.UseVisualStyleBackColor = true;
            this.btnResM4.Click += new System.EventHandler(this.btnResM_Click);
            // 
            // btnOcuM4
            // 
            this.btnOcuM4.Location = new System.Drawing.Point(453, 11);
            this.btnOcuM4.Name = "btnOcuM4";
            this.btnOcuM4.Size = new System.Drawing.Size(100, 35);
            this.btnOcuM4.TabIndex = 3;
            this.btnOcuM4.Text = "Ocupar";
            this.btnOcuM4.UseVisualStyleBackColor = true;
            this.btnOcuM4.Click += new System.EventHandler(this.btnOcuM_Click);
            // 
            // btnResM3
            // 
            this.btnResM3.Location = new System.Drawing.Point(307, 52);
            this.btnResM3.Name = "btnResM3";
            this.btnResM3.Size = new System.Drawing.Size(100, 35);
            this.btnResM3.TabIndex = 3;
            this.btnResM3.Text = "Reservar";
            this.btnResM3.UseVisualStyleBackColor = true;
            this.btnResM3.Click += new System.EventHandler(this.btnResM_Click);
            // 
            // btnOcuM3
            // 
            this.btnOcuM3.Location = new System.Drawing.Point(307, 11);
            this.btnOcuM3.Name = "btnOcuM3";
            this.btnOcuM3.Size = new System.Drawing.Size(100, 35);
            this.btnOcuM3.TabIndex = 3;
            this.btnOcuM3.Text = "Ocupar";
            this.btnOcuM3.UseVisualStyleBackColor = true;
            this.btnOcuM3.Click += new System.EventHandler(this.btnOcuM_Click);
            // 
            // btnResM2
            // 
            this.btnResM2.Location = new System.Drawing.Point(153, 52);
            this.btnResM2.Name = "btnResM2";
            this.btnResM2.Size = new System.Drawing.Size(100, 35);
            this.btnResM2.TabIndex = 3;
            this.btnResM2.Text = "Reservar";
            this.btnResM2.UseVisualStyleBackColor = true;
            this.btnResM2.Click += new System.EventHandler(this.btnResM_Click);
            // 
            // btnOcuM2
            // 
            this.btnOcuM2.Location = new System.Drawing.Point(153, 11);
            this.btnOcuM2.Name = "btnOcuM2";
            this.btnOcuM2.Size = new System.Drawing.Size(100, 35);
            this.btnOcuM2.TabIndex = 3;
            this.btnOcuM2.Text = "Ocupar";
            this.btnOcuM2.UseVisualStyleBackColor = true;
            this.btnOcuM2.Click += new System.EventHandler(this.btnOcuM_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(804, 601);
            this.Controls.Add(this.panelBotones);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.panelMesas);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(820, 640);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(820, 640);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Restaurante";
            this.panelMesas.ResumeLayout(false);
            this.panelMesas.PerformLayout();
            this.panelBotones.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMesas;
        private System.Windows.Forms.Label lblMesa1;
        private System.Windows.Forms.Label Mesa1;
        private System.Windows.Forms.Label lblMesa2;
        private System.Windows.Forms.Label Mesa2;
        private System.Windows.Forms.Label lblMesa3;
        private System.Windows.Forms.Label Mesa3;
        private System.Windows.Forms.Label lblMesa4;
        private System.Windows.Forms.Label Mesa4;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnOcuM1;
        private System.Windows.Forms.Button btnResM1;
        private System.Windows.Forms.Button btnDesM1;
        private System.Windows.Forms.Panel panelBotones;
        private System.Windows.Forms.Button btnDesM4;
        private System.Windows.Forms.Button btnDesM3;
        private System.Windows.Forms.Button btnDesM2;
        private System.Windows.Forms.Button btnResM4;
        private System.Windows.Forms.Button btnOcuM4;
        private System.Windows.Forms.Button btnResM3;
        private System.Windows.Forms.Button btnOcuM3;
        private System.Windows.Forms.Button btnResM2;
        private System.Windows.Forms.Button btnOcuM2;
    }
}

