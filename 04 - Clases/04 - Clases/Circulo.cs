﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04___Clases
{
    class Circulo
    {
        private int radio;
        private string color;

        public Circulo(int radio, string color)
        {
            this.radio = radio;
            this.color = color;
        }

        public string getColor()
        {
            return this.color;
        }

        public double Area()
        {
            return Math.PI * radio * radio;
        }
    }
}
