﻿namespace _04___Clases
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.txtRadio1 = new System.Windows.Forms.TextBox();
            this.txtColor1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtRadio2 = new System.Windows.Forms.TextBox();
            this.txtColor2 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.txtRadio3 = new System.Windows.Forms.TextBox();
            this.txtColor3 = new System.Windows.Forms.TextBox();
            this.lblC1 = new System.Windows.Forms.Label();
            this.lblC2 = new System.Windows.Forms.Label();
            this.lblC3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(41, 177);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 55);
            this.button1.TabIndex = 1;
            this.button1.Text = "Circulo 1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtRadio1
            // 
            this.txtRadio1.Location = new System.Drawing.Point(41, 45);
            this.txtRadio1.Name = "txtRadio1";
            this.txtRadio1.Size = new System.Drawing.Size(156, 29);
            this.txtRadio1.TabIndex = 2;
            // 
            // txtColor1
            // 
            this.txtColor1.Location = new System.Drawing.Point(41, 100);
            this.txtColor1.Name = "txtColor1";
            this.txtColor1.Size = new System.Drawing.Size(156, 29);
            this.txtColor1.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(320, 177);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(156, 55);
            this.button2.TabIndex = 1;
            this.button2.Text = "Circulo 2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtRadio2
            // 
            this.txtRadio2.Location = new System.Drawing.Point(320, 45);
            this.txtRadio2.Name = "txtRadio2";
            this.txtRadio2.Size = new System.Drawing.Size(156, 29);
            this.txtRadio2.TabIndex = 2;
            // 
            // txtColor2
            // 
            this.txtColor2.Location = new System.Drawing.Point(320, 100);
            this.txtColor2.Name = "txtColor2";
            this.txtColor2.Size = new System.Drawing.Size(156, 29);
            this.txtColor2.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(603, 177);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(156, 55);
            this.button3.TabIndex = 1;
            this.button3.Text = "Circulo 3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtRadio3
            // 
            this.txtRadio3.Location = new System.Drawing.Point(603, 45);
            this.txtRadio3.Name = "txtRadio3";
            this.txtRadio3.Size = new System.Drawing.Size(156, 29);
            this.txtRadio3.TabIndex = 2;
            // 
            // txtColor3
            // 
            this.txtColor3.Location = new System.Drawing.Point(603, 100);
            this.txtColor3.Name = "txtColor3";
            this.txtColor3.Size = new System.Drawing.Size(156, 29);
            this.txtColor3.TabIndex = 3;
            // 
            // lblC1
            // 
            this.lblC1.AutoSize = true;
            this.lblC1.Location = new System.Drawing.Point(37, 259);
            this.lblC1.Name = "lblC1";
            this.lblC1.Size = new System.Drawing.Size(0, 24);
            this.lblC1.TabIndex = 4;
            // 
            // lblC2
            // 
            this.lblC2.AutoSize = true;
            this.lblC2.Location = new System.Drawing.Point(316, 259);
            this.lblC2.Name = "lblC2";
            this.lblC2.Size = new System.Drawing.Size(0, 24);
            this.lblC2.TabIndex = 4;
            // 
            // lblC3
            // 
            this.lblC3.AutoSize = true;
            this.lblC3.Location = new System.Drawing.Point(599, 259);
            this.lblC3.Name = "lblC3";
            this.lblC3.Size = new System.Drawing.Size(0, 24);
            this.lblC3.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 434);
            this.Controls.Add(this.lblC3);
            this.Controls.Add(this.lblC2);
            this.Controls.Add(this.lblC1);
            this.Controls.Add(this.txtColor3);
            this.Controls.Add(this.txtColor2);
            this.Controls.Add(this.txtColor1);
            this.Controls.Add(this.txtRadio3);
            this.Controls.Add(this.txtRadio2);
            this.Controls.Add(this.txtRadio1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtRadio1;
        private System.Windows.Forms.TextBox txtColor1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtRadio2;
        private System.Windows.Forms.TextBox txtColor2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtRadio3;
        private System.Windows.Forms.TextBox txtColor3;
        private System.Windows.Forms.Label lblC1;
        private System.Windows.Forms.Label lblC2;
        private System.Windows.Forms.Label lblC3;
    }
}

