﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _04___Clases
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Circulo c1 = new Circulo(Int32.Parse(txtRadio1.Text), txtColor1.Text);
            double areaC1 = c1.Area();

            lblC1.Text = areaC1.ToString() + "\n" + c1.getColor();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Circulo c2 = new Circulo(Int32.Parse(txtRadio2.Text), txtColor2.Text);
            double areaC2 = c2.Area();

            lblC2.Text = areaC2.ToString() + "\n" + c2.getColor();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Circulo c3 = new Circulo(Int32.Parse(txtRadio3.Text), txtColor3.Text);
            double areaC3 = c3.Area();

            lblC3.Text = areaC3.ToString() + "\n" + c3.getColor();
        }
    }
}
