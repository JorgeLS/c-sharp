﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01___C.A.Tipos
{
    class Program
    {
        static void Main(string[] args)
        {
            Type tipo;

            tipo = typeof(string); // Asigna un tipo

            Console.WriteLine("El nombre es " + tipo.Name + " y mas largo " + tipo.FullName);
            Console.WriteLine("Hola Mundo!!!");
            Console.ReadKey();
        }
    }
}
