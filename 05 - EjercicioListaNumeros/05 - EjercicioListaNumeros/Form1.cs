﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _05___EjercicioListaNumeros
{
    public partial class Form1 : Form
    {
        private ListaNumeros lista10;
        private ListaNumeros lista15;

        public Form1()
        {
            InitializeComponent();

            lista10 = new ListaNumeros();
            lista15 = new ListaNumeros();
        }

        private void btnCrear10_Click(object sender, EventArgs e)
        {
            // Vacio la variable para que no se sume
            if (!lista10.Vacia())
                lista10.Vaciar();

            txt10.Text = "";

            Random rdn = new Random();

            for (int i = 0; i < 10; i++)
                lista10.Insertar(rdn.Next(1, 30));

            for (int i = 0; i < lista10.NumeroElementos(); i++)
                txt10.Text += lista10.Elemento(i) + " ";
        }

        private void btnCrear15_Click(object sender, EventArgs e)
        {
            // Vacio la variable para que no se sume
            if (!lista15.Vacia())
                lista15.Vaciar();

            txt15.Text = "";

            Random rdn = new Random();

            for (int i = 0; i < 15; i++)
                lista15.Insertar(rdn.Next(1, 30));

            for (int i = 0; i < lista15.NumeroElementos(); i++)
                txt15.Text += lista15.Elemento(i) + " ";
        }

        private void btnVaciar10_Click(object sender, EventArgs e)
        {
            if (!lista10.Vacia())
                lista10.Vaciar();

            txt10.Text = "";
        }
        private void btnVaciar15_Click_1(object sender, EventArgs e)
        {
            if (!lista15.Vacia())
                lista15.Vaciar();

            txt15.Text = "";
        }

        private void btnInverso15_Click(object sender, EventArgs e)
        {
            txt15.Text = "";

            for (int i = lista15.NumeroElementos() - 1; i >= 0; i--)
                txt15.Text += lista15.Elemento(i) + " ";
        }

        private void btnMostrar10_Click(object sender, EventArgs e)
        {
            txt10.Text = "";

            ListaNumeros lista = new ListaNumeros();
            lista.Vaciar();

            for (int i = 0; i < lista10.NumeroElementos(); i++)
                lista.Insertar( lista10.Elemento(i) );

            for (int i = lista.NumeroElementos()-1; i >= 0; i--)
                txt10.Text += lista.Sacar() + " ";
        }

        private void btnMostrarIn10_Click(object sender, EventArgs e)
        {
            txt10.Text = "";

            ListaNumeros lista = new ListaNumeros();
            lista.Vaciar();

            for (int i = lista10.NumeroElementos() - 1; i >= 0; i--)
                lista.Insertar(lista10.Elemento(i));

            for (int i = lista.NumeroElementos()-1; i >= 0; i--)
                txt10.Text += lista.Sacar() + " ";
        }

        private void btnPares10_Click(object sender, EventArgs e)
        {
            txt10.Text = "";
            for (int i = 0; i < lista10.NumeroElementos(); i++)
                if (lista10.Elemento(i) % 2 == 0)
                    txt10.Text += lista10.Elemento(i) + " ";
        }

        private void btnImpares10_Click(object sender, EventArgs e)
        {
            txt10.Text = "";
            for (int i = 0; i < lista10.NumeroElementos(); i++)
                if (lista10.Elemento(i) % 2 != 0)
                    txt10.Text += lista10.Elemento(i) + " ";
        }

        private void btnMostrar115_Click(object sender, EventArgs e)
        {
            txt15.Text = "";
            for (int i = 0; i < lista15.NumeroElementos(); i++)
                if (lista15.Elemento(i) < 10)
                    txt15.Text += lista15.Elemento(i) + " ";
        }

        private void btnMostrar215_Click(object sender, EventArgs e)
        {
            txt15.Text = "";
            for (int i = 0; i < lista15.NumeroElementos(); i++)
                if (lista15.Elemento(i) >= 10)
                    txt15.Text += lista15.Elemento(i) + " ";
        }

        private void btnIguales_Click(object sender, EventArgs e)
        {
            txtIguales.Text = "";
            for (int i = 0; i < lista10.NumeroElementos(); i++)
            {
                if (lista15.Posicion( lista10.Elemento(i) ) != -1)
                {
                    txtIguales.Text += lista10.Elemento(i) + " ";
                }
            }
        }

        private void btnNoIguales_Click(object sender, EventArgs e)
        {
            txtIguales.Text = "";

            // Primero comprueba la lista de 10 numeros
            for (int i = 0; i < lista10.NumeroElementos(); i++)
            {
                if (lista15.Posicion(lista10.Elemento(i)) == -1)
                {
                    txtIguales.Text += lista10.Elemento(i) + " ";
                }
            }

            // Despues comprueba la lista de 15 numeros
            for (int i = 0; i < lista15.NumeroElementos(); i++)
            {
                if (lista10.Posicion( lista15.Elemento(i) ) == -1)
                {
                    txtIguales.Text += lista15.Elemento(i) + " ";
                }
            }
        }
    }
}
