﻿namespace _05___EjercicioListaNumeros
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCrear10 = new System.Windows.Forms.Button();
            this.lbl101 = new System.Windows.Forms.Label();
            this.lbl102 = new System.Windows.Forms.Label();
            this.lbl151 = new System.Windows.Forms.Label();
            this.lbl152 = new System.Windows.Forms.Label();
            this.btnVaciar10 = new System.Windows.Forms.Button();
            this.btnCrear15 = new System.Windows.Forms.Button();
            this.btnVaciar15 = new System.Windows.Forms.Button();
            this.btnInverso15 = new System.Windows.Forms.Button();
            this.txt15 = new System.Windows.Forms.TextBox();
            this.txt10 = new System.Windows.Forms.TextBox();
            this.btnMostrar10 = new System.Windows.Forms.Button();
            this.btnMostrarIn10 = new System.Windows.Forms.Button();
            this.btnPares10 = new System.Windows.Forms.Button();
            this.btnImpares10 = new System.Windows.Forms.Button();
            this.btnMostrar115 = new System.Windows.Forms.Button();
            this.btnMostrar215 = new System.Windows.Forms.Button();
            this.txtIguales = new System.Windows.Forms.TextBox();
            this.btnIguales = new System.Windows.Forms.Button();
            this.btnNoIguales = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCrear10
            // 
            this.btnCrear10.Location = new System.Drawing.Point(39, 112);
            this.btnCrear10.Margin = new System.Windows.Forms.Padding(4);
            this.btnCrear10.Name = "btnCrear10";
            this.btnCrear10.Size = new System.Drawing.Size(137, 47);
            this.btnCrear10.TabIndex = 0;
            this.btnCrear10.Text = "Pon 10 Num";
            this.btnCrear10.UseVisualStyleBackColor = true;
            this.btnCrear10.Click += new System.EventHandler(this.btnCrear10_Click);
            // 
            // lbl101
            // 
            this.lbl101.AutoSize = true;
            this.lbl101.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl101.Location = new System.Drawing.Point(33, 46);
            this.lbl101.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl101.Name = "lbl101";
            this.lbl101.Size = new System.Drawing.Size(171, 24);
            this.lbl101.TabIndex = 1;
            this.lbl101.Text = "Lista Numeros 10";
            // 
            // lbl102
            // 
            this.lbl102.AutoSize = true;
            this.lbl102.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl102.Location = new System.Drawing.Point(33, 305);
            this.lbl102.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl102.Name = "lbl102";
            this.lbl102.Size = new System.Drawing.Size(58, 24);
            this.lbl102.TabIndex = 2;
            this.lbl102.Text = "Lista:";
            // 
            // lbl151
            // 
            this.lbl151.AutoSize = true;
            this.lbl151.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl151.Location = new System.Drawing.Point(533, 46);
            this.lbl151.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl151.Name = "lbl151";
            this.lbl151.Size = new System.Drawing.Size(171, 24);
            this.lbl151.TabIndex = 1;
            this.lbl151.Text = "Lista Numeros 15";
            // 
            // lbl152
            // 
            this.lbl152.AutoSize = true;
            this.lbl152.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl152.Location = new System.Drawing.Point(529, 305);
            this.lbl152.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl152.Name = "lbl152";
            this.lbl152.Size = new System.Drawing.Size(58, 24);
            this.lbl152.TabIndex = 2;
            this.lbl152.Text = "Lista:";
            // 
            // btnVaciar10
            // 
            this.btnVaciar10.Location = new System.Drawing.Point(223, 112);
            this.btnVaciar10.Margin = new System.Windows.Forms.Padding(4);
            this.btnVaciar10.Name = "btnVaciar10";
            this.btnVaciar10.Size = new System.Drawing.Size(135, 47);
            this.btnVaciar10.TabIndex = 0;
            this.btnVaciar10.Text = "Vacia 10";
            this.btnVaciar10.UseVisualStyleBackColor = true;
            this.btnVaciar10.Click += new System.EventHandler(this.btnVaciar10_Click);
            // 
            // btnCrear15
            // 
            this.btnCrear15.Location = new System.Drawing.Point(535, 112);
            this.btnCrear15.Margin = new System.Windows.Forms.Padding(4);
            this.btnCrear15.Name = "btnCrear15";
            this.btnCrear15.Size = new System.Drawing.Size(137, 47);
            this.btnCrear15.TabIndex = 0;
            this.btnCrear15.Text = "Pon 15 Num";
            this.btnCrear15.UseVisualStyleBackColor = true;
            this.btnCrear15.Click += new System.EventHandler(this.btnCrear15_Click);
            // 
            // btnVaciar15
            // 
            this.btnVaciar15.Location = new System.Drawing.Point(719, 112);
            this.btnVaciar15.Margin = new System.Windows.Forms.Padding(4);
            this.btnVaciar15.Name = "btnVaciar15";
            this.btnVaciar15.Size = new System.Drawing.Size(135, 47);
            this.btnVaciar15.TabIndex = 0;
            this.btnVaciar15.Text = "Vacia 15";
            this.btnVaciar15.UseVisualStyleBackColor = true;
            this.btnVaciar15.Click += new System.EventHandler(this.btnVaciar15_Click_1);
            // 
            // btnInverso15
            // 
            this.btnInverso15.Location = new System.Drawing.Point(537, 167);
            this.btnInverso15.Margin = new System.Windows.Forms.Padding(4);
            this.btnInverso15.Name = "btnInverso15";
            this.btnInverso15.Size = new System.Drawing.Size(135, 47);
            this.btnInverso15.TabIndex = 3;
            this.btnInverso15.Text = "Orden Inverso";
            this.btnInverso15.UseVisualStyleBackColor = true;
            this.btnInverso15.Click += new System.EventHandler(this.btnInverso15_Click);
            // 
            // txt15
            // 
            this.txt15.Location = new System.Drawing.Point(535, 351);
            this.txt15.Margin = new System.Windows.Forms.Padding(4);
            this.txt15.Name = "txt15";
            this.txt15.Size = new System.Drawing.Size(317, 22);
            this.txt15.TabIndex = 4;
            // 
            // txt10
            // 
            this.txt10.Location = new System.Drawing.Point(39, 351);
            this.txt10.Margin = new System.Windows.Forms.Padding(4);
            this.txt10.Name = "txt10";
            this.txt10.Size = new System.Drawing.Size(317, 22);
            this.txt10.TabIndex = 4;
            // 
            // btnMostrar10
            // 
            this.btnMostrar10.Location = new System.Drawing.Point(39, 167);
            this.btnMostrar10.Margin = new System.Windows.Forms.Padding(4);
            this.btnMostrar10.Name = "btnMostrar10";
            this.btnMostrar10.Size = new System.Drawing.Size(137, 47);
            this.btnMostrar10.TabIndex = 0;
            this.btnMostrar10.Text = "Muestra";
            this.btnMostrar10.UseVisualStyleBackColor = true;
            this.btnMostrar10.Click += new System.EventHandler(this.btnMostrar10_Click);
            // 
            // btnMostrarIn10
            // 
            this.btnMostrarIn10.Location = new System.Drawing.Point(223, 167);
            this.btnMostrarIn10.Margin = new System.Windows.Forms.Padding(4);
            this.btnMostrarIn10.Name = "btnMostrarIn10";
            this.btnMostrarIn10.Size = new System.Drawing.Size(137, 47);
            this.btnMostrarIn10.TabIndex = 0;
            this.btnMostrarIn10.Text = "Muestra Inverso";
            this.btnMostrarIn10.UseVisualStyleBackColor = true;
            this.btnMostrarIn10.Click += new System.EventHandler(this.btnMostrarIn10_Click);
            // 
            // btnPares10
            // 
            this.btnPares10.Location = new System.Drawing.Point(39, 222);
            this.btnPares10.Margin = new System.Windows.Forms.Padding(4);
            this.btnPares10.Name = "btnPares10";
            this.btnPares10.Size = new System.Drawing.Size(137, 47);
            this.btnPares10.TabIndex = 0;
            this.btnPares10.Text = "Muestra Pares";
            this.btnPares10.UseVisualStyleBackColor = true;
            this.btnPares10.Click += new System.EventHandler(this.btnPares10_Click);
            // 
            // btnImpares10
            // 
            this.btnImpares10.Location = new System.Drawing.Point(223, 222);
            this.btnImpares10.Margin = new System.Windows.Forms.Padding(4);
            this.btnImpares10.Name = "btnImpares10";
            this.btnImpares10.Size = new System.Drawing.Size(137, 47);
            this.btnImpares10.TabIndex = 0;
            this.btnImpares10.Text = "Muestra Impares";
            this.btnImpares10.UseVisualStyleBackColor = true;
            this.btnImpares10.Click += new System.EventHandler(this.btnImpares10_Click);
            // 
            // btnMostrar115
            // 
            this.btnMostrar115.Location = new System.Drawing.Point(719, 167);
            this.btnMostrar115.Margin = new System.Windows.Forms.Padding(4);
            this.btnMostrar115.Name = "btnMostrar115";
            this.btnMostrar115.Size = new System.Drawing.Size(135, 47);
            this.btnMostrar115.TabIndex = 3;
            this.btnMostrar115.Text = "Mostrar 1 Digito";
            this.btnMostrar115.UseVisualStyleBackColor = true;
            this.btnMostrar115.Click += new System.EventHandler(this.btnMostrar115_Click);
            // 
            // btnMostrar215
            // 
            this.btnMostrar215.Location = new System.Drawing.Point(537, 222);
            this.btnMostrar215.Margin = new System.Windows.Forms.Padding(4);
            this.btnMostrar215.Name = "btnMostrar215";
            this.btnMostrar215.Size = new System.Drawing.Size(135, 47);
            this.btnMostrar215.TabIndex = 3;
            this.btnMostrar215.Text = "Mostrar 2 Digitos";
            this.btnMostrar215.UseVisualStyleBackColor = true;
            this.btnMostrar215.Click += new System.EventHandler(this.btnMostrar215_Click);
            // 
            // txtIguales
            // 
            this.txtIguales.Location = new System.Drawing.Point(533, 470);
            this.txtIguales.Margin = new System.Windows.Forms.Padding(4);
            this.txtIguales.Name = "txtIguales";
            this.txtIguales.Size = new System.Drawing.Size(317, 22);
            this.txtIguales.TabIndex = 4;
            // 
            // btnIguales
            // 
            this.btnIguales.Location = new System.Drawing.Point(39, 458);
            this.btnIguales.Margin = new System.Windows.Forms.Padding(4);
            this.btnIguales.Name = "btnIguales";
            this.btnIguales.Size = new System.Drawing.Size(137, 47);
            this.btnIguales.TabIndex = 0;
            this.btnIguales.Text = "Muestra Iguales";
            this.btnIguales.UseVisualStyleBackColor = true;
            this.btnIguales.Click += new System.EventHandler(this.btnIguales_Click);
            // 
            // btnNoIguales
            // 
            this.btnNoIguales.Location = new System.Drawing.Point(223, 458);
            this.btnNoIguales.Margin = new System.Windows.Forms.Padding(4);
            this.btnNoIguales.Name = "btnNoIguales";
            this.btnNoIguales.Size = new System.Drawing.Size(137, 47);
            this.btnNoIguales.TabIndex = 0;
            this.btnNoIguales.Text = "Muestra No Iguales";
            this.btnNoIguales.UseVisualStyleBackColor = true;
            this.btnNoIguales.Click += new System.EventHandler(this.btnNoIguales_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(1054, 545);
            this.Controls.Add(this.txt10);
            this.Controls.Add(this.txtIguales);
            this.Controls.Add(this.txt15);
            this.Controls.Add(this.btnMostrar215);
            this.Controls.Add(this.btnMostrar115);
            this.Controls.Add(this.btnInverso15);
            this.Controls.Add(this.lbl152);
            this.Controls.Add(this.lbl102);
            this.Controls.Add(this.lbl151);
            this.Controls.Add(this.lbl101);
            this.Controls.Add(this.btnVaciar15);
            this.Controls.Add(this.btnCrear15);
            this.Controls.Add(this.btnVaciar10);
            this.Controls.Add(this.btnNoIguales);
            this.Controls.Add(this.btnIguales);
            this.Controls.Add(this.btnImpares10);
            this.Controls.Add(this.btnPares10);
            this.Controls.Add(this.btnMostrarIn10);
            this.Controls.Add(this.btnMostrar10);
            this.Controls.Add(this.btnCrear10);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Ejercicio Clases Lista Numeros";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCrear10;
        private System.Windows.Forms.Label lbl101;
        private System.Windows.Forms.Label lbl102;
        private System.Windows.Forms.Label lbl151;
        private System.Windows.Forms.Label lbl152;
        private System.Windows.Forms.Button btnVaciar10;
        private System.Windows.Forms.Button btnCrear15;
        private System.Windows.Forms.Button btnVaciar15;
        private System.Windows.Forms.Button btnInverso15;
        private System.Windows.Forms.TextBox txt15;
        private System.Windows.Forms.TextBox txt10;
        private System.Windows.Forms.Button btnMostrar10;
        private System.Windows.Forms.Button btnMostrarIn10;
        private System.Windows.Forms.Button btnPares10;
        private System.Windows.Forms.Button btnImpares10;
        private System.Windows.Forms.Button btnMostrar115;
        private System.Windows.Forms.Button btnMostrar215;
        private System.Windows.Forms.TextBox txtIguales;
        private System.Windows.Forms.Button btnIguales;
        private System.Windows.Forms.Button btnNoIguales;
    }
}

