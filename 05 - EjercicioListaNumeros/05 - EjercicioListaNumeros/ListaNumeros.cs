﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05___EjercicioListaNumeros
{
    class ListaNumeros
    {
        private int numElementos;
        private int[] elementos;

        public ListaNumeros ()
        {
            this.numElementos = 0;
            this.elementos = new int[100];
        }

        public ListaNumeros (int numero, int valor) : base() // or this() // Llama al constructor () si se pone base() llama al constructor del padre y el this() al mismo
        {
            /*
             * Al poner : base() or : this() no hace falta poner esto
             * this.numElementos = numero;
             * this.elementos = new int[numElementos];
            */
            for (int i = 0; i < numero; i++)
                elementos[i] = valor;
        }

        public int NumeroElementos()
        {
            return numElementos;
        }

        public void Insertar(int valor)
        {
            if (NumeroElementos() < 100 || valor >= 0)
                elementos[numElementos++] = valor;
        }

        public int Primero()
        {
            if (NumeroElementos() == 0)
                return -1000;
            else
                return elementos[0];
        }

        public int Ultimo()
        {
            if (NumeroElementos() > 0)
                return elementos[NumeroElementos() - 1];
            else
                return -1000;
        }

        public int Sacar()
        {
            int valorSacado;

            if (NumeroElementos() > 0)
            {
                valorSacado = Primero();

                for (int i = 0; i < NumeroElementos(); i++)
                {
                    if (NumeroElementos() != NumeroElementos() - 1)
                        elementos[i] = elementos[i + 1];
                    else
                        elementos[i] = 0;
                }
                numElementos--;

                return valorSacado;
            }
            else
                return -1000;
        }

        public int Elemento(int pos)
        {
            if (NumeroElementos() > 0 && (pos >= NumeroElementos() || pos < 0))
                return -1000;
            else
                return elementos[pos];
        }

        public int Posicion(int valor)
        {
            if (NumeroElementos() > 0)
            {
                for (int i = 0; i < NumeroElementos(); i++)
                    if (elementos[i] == valor)
                        return i;
            }
            return -1;
        }

        public bool Vacia()
        {
            if (elementos.Length == 0)
                return true;
            else
                return false;
        }

        public void Vaciar()
        {
            elementos = new int[100];
            numElementos = 0;
        }
    }
}
