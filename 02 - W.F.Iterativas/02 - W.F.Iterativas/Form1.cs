﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02___W.F.Iterativas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnIN_Click(object sender, EventArgs e)
        {
            int numero;

            numero = Int32.Parse(txtNum.Text);

            txtResultadoNum.Text = "";

            for (int i = 0; i <= numero; i++)
            {
                txtResultadoNum.Text += i;
                if ( i != numero)
                    txtResultadoNum.Text += " - ";
            }
        }

        private void txtResultado_Click(object sender, EventArgs e)
        {

        }

        private void btnTabla_Click(object sender, EventArgs e)
        {
            int[] tabla = { 11, 22, 33, 44, 55 };

            txtResultadoTabla.Text = "";

            /*
            for (int i = 0; i < tabla.Length; i++)
                txtResultadoTabla.Text += tabla[i] + " ";
            */

            foreach (int num in tabla) {
                txtResultadoTabla.Text += num + " ";
            }
        }
    }
}
