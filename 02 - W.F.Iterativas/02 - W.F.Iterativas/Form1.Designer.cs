﻿namespace _02___W.F.Iterativas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNum = new System.Windows.Forms.TextBox();
            this.btnIN = new System.Windows.Forms.Button();
            this.txtResultadoNum = new System.Windows.Forms.Label();
            this.btnTabla = new System.Windows.Forms.Button();
            this.txtResultadoTabla = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtNum
            // 
            this.txtNum.Location = new System.Drawing.Point(463, 149);
            this.txtNum.Name = "txtNum";
            this.txtNum.Size = new System.Drawing.Size(320, 26);
            this.txtNum.TabIndex = 0;
            this.txtNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNum.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnIN
            // 
            this.btnIN.Location = new System.Drawing.Point(839, 140);
            this.btnIN.Name = "btnIN";
            this.btnIN.Size = new System.Drawing.Size(156, 34);
            this.btnIN.TabIndex = 1;
            this.btnIN.Text = "Imprimir Números";
            this.btnIN.UseVisualStyleBackColor = true;
            this.btnIN.Click += new System.EventHandler(this.btnIN_Click);
            // 
            // txtResultadoNum
            // 
            this.txtResultadoNum.AutoSize = true;
            this.txtResultadoNum.BackColor = System.Drawing.Color.DarkGray;
            this.txtResultadoNum.Location = new System.Drawing.Point(460, 252);
            this.txtResultadoNum.MinimumSize = new System.Drawing.Size(200, 0);
            this.txtResultadoNum.Name = "txtResultadoNum";
            this.txtResultadoNum.Size = new System.Drawing.Size(200, 18);
            this.txtResultadoNum.TabIndex = 2;
            this.txtResultadoNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtResultadoNum.Click += new System.EventHandler(this.txtResultado_Click);
            // 
            // btnTabla
            // 
            this.btnTabla.Location = new System.Drawing.Point(839, 321);
            this.btnTabla.Name = "btnTabla";
            this.btnTabla.Size = new System.Drawing.Size(156, 34);
            this.btnTabla.TabIndex = 1;
            this.btnTabla.Text = "Imprimir Tabla";
            this.btnTabla.UseVisualStyleBackColor = true;
            this.btnTabla.Click += new System.EventHandler(this.btnTabla_Click);
            // 
            // txtResultadoTabla
            // 
            this.txtResultadoTabla.AutoSize = true;
            this.txtResultadoTabla.BackColor = System.Drawing.Color.DarkGray;
            this.txtResultadoTabla.Location = new System.Drawing.Point(460, 337);
            this.txtResultadoTabla.MinimumSize = new System.Drawing.Size(200, 0);
            this.txtResultadoTabla.Name = "txtResultadoTabla";
            this.txtResultadoTabla.Size = new System.Drawing.Size(200, 18);
            this.txtResultadoTabla.TabIndex = 2;
            this.txtResultadoTabla.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtResultadoTabla.Click += new System.EventHandler(this.txtResultado_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.ClientSize = new System.Drawing.Size(1322, 764);
            this.Controls.Add(this.txtResultadoTabla);
            this.Controls.Add(this.txtResultadoNum);
            this.Controls.Add(this.btnTabla);
            this.Controls.Add(this.btnIN);
            this.Controls.Add(this.txtNum);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Sentencias Iterativas";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNum;
        private System.Windows.Forms.Button btnIN;
        private System.Windows.Forms.Label txtResultadoNum;
        private System.Windows.Forms.Button btnTabla;
        private System.Windows.Forms.Label txtResultadoTabla;
    }
}

