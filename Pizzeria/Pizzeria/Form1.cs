﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzeria
{
    public partial class Formulario : Form
    {
        int num_ingdExtra;
        string[] nom_indgExtra; 
        Pizzeria.Ingrediente[] ingrediente;

        public Formulario()
        {
            InitializeComponent();
            inicializaIngredientes();
            pizza.Image = Image.FromFile(@"..\..\Resources\logoPizzeria.jpg");
        }

        private void cBEligePizza_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cBEligePizza.Text)
            {
                case "New York":
                    // Resetea todos los ingredientes
                    volverNormalPizza();
                    // Añade y fija los ingredientes a elegir
                    ingredientesTodosEnabled();
                    ingrediente[0].fijarIngrediente();
                    ingrediente[1].fijarIngrediente();
                    nom_indgExtra[num_ingdExtra++] = ingrediente[0].Nombre;
                    nom_indgExtra[num_ingdExtra++] = ingrediente[1].Nombre;
                    // Añade el tamanio y la masa
                    cBTamanio.Enabled = true;
                    cBMasa.Enabled = true;
                    // Cambia la imagen de la pizza
                    pizza.Image = Image.FromFile(@"..\..\Resources\pizzaNY.png");;
                    break;
                case "Vegetariana":
                    // Resetea todos los ingredientes
                    volverNormalPizza();
                    // Añade y fija los ingredientes
                    ingredientesVegetarianosEnabled(true);
                    ingredientesNoVegetarianosEnabled(false);
                    ingrediente[2].fijarIngrediente();
                    ingrediente[3].fijarIngrediente();
                    nom_indgExtra[num_ingdExtra++] = ingrediente[2].Nombre;
                    nom_indgExtra[num_ingdExtra++] = ingrediente[3].Nombre;
                    // Añade el tamanio y la masa
                    cBTamanio.Enabled = false;
                    cBMasa.Enabled = true;
                    cBTamanio.SelectedIndex = 0;
                    // Cambia la imagen de la pizza
                    pizza.Image = Image.FromFile(@"..\..\Resources\pizzaVe.png"); ;
                    break;
                case "Barbacoa Picante":
                    // Resetea todos los ingredientes
                    volverNormalPizza();
                    // Añade y fija los ingredientes
                    ingredientesTodosEnabled();
                    ingrediente[4].fijarIngrediente();
                    ingrediente[5].fijarIngrediente();
                    nom_indgExtra[num_ingdExtra++] = ingrediente[4].Nombre;
                    nom_indgExtra[num_ingdExtra++] = ingrediente[5].Nombre;
                    // Añade el tamanio y la masa
                    cBTamanio.Enabled = true;
                    cBMasa.Enabled = true;
                    // Cambia la imagen de la pizza
                    pizza.Image = Image.FromFile(@"..\..\Resources\pizzaBP.png");
                    break;
                case "4 Quesos":
                    // Resetea todos los ingredientes
                    volverNormalPizza();
                    // Añade y fija los ingredientes
                    ingredientesTodosEnabled();
                    ingrediente[6].fijarIngrediente();
                    ingrediente[7].fijarIngrediente();
                    ingrediente[8].fijarIngrediente();
                    ingrediente[9].fijarIngrediente();
                    nom_indgExtra[num_ingdExtra++] = ingrediente[6].Nombre;
                    nom_indgExtra[num_ingdExtra++] = ingrediente[7].Nombre;
                    nom_indgExtra[num_ingdExtra++] = ingrediente[8].Nombre;
                    nom_indgExtra[num_ingdExtra++] = ingrediente[9].Nombre;
                    for (int i = 0; i < ingrediente.Length; i++)
                    {
                        ingrediente[i].CheckBox.AutoCheck = false;
                        if (!ingrediente[i].CheckBox.Checked)
                            ingrediente[i].CheckBox.Enabled = false;
                    }
                    // Añade el tamanio y la masa
                    cBTamanio.Enabled = true;
                    cBMasa.Enabled = false;
                    cBMasa.SelectedIndex = 0;
                    // Cambia la imagen de la pizza
                    pizza.Image = Image.FromFile(@"..\..\Resources\pizza4Q.png");
                    break;
                default:
                    // Esta opcion no deberia de pasar nunca, solo la pongo por si acaso
                    Console.WriteLine("Valor seleccionado en cBEligePizza incorrecto");
                    Application.Exit();
                    break;
            }
            addResumen();
        }

        private void inicializaIngredientes()
        {
            Control.ControlCollection ingds = panelIngd.Controls;
            int numeroIngredientes = ingds.Count;
            Console.WriteLine(ingds[0]);
            
            ingrediente = new Ingrediente[numeroIngredientes];
            nom_indgExtra = new string[numeroIngredientes];
            num_ingdExtra = 0;

            for (int i = 0; i < numeroIngredientes; i++)
            {
                ingrediente[i] = new Ingrediente((CheckBox)ingds[i], ingds[i].Text);
            }
        }

        private void volverNormalPizza()
        {
            for (int i = 0; i < ingrediente.Length; i++)
            {
                ingrediente[i].resetear();
            }

            num_ingdExtra = 0;

            cBMasa.Enabled = false;
            cBTamanio.Enabled = false;
        }

        private void ingredientesNoVegetarianosEnabled(bool estado)
        {

            ingrediente[0].CheckBox.Enabled = estado;
            ingrediente[5].CheckBox.Enabled = estado;
            ingrediente[12].CheckBox.Enabled = estado;
            ingrediente[11].CheckBox.Enabled = estado;
        }

        private void ingredientesVegetarianosEnabled(bool estado)
        {
            ingrediente[1].CheckBox.AutoCheck = estado;
            ingrediente[2].CheckBox.AutoCheck = estado;
            ingrediente[3].CheckBox.AutoCheck = estado;
            ingrediente[4].CheckBox.AutoCheck = estado;
            ingrediente[6].CheckBox.Enabled = estado;
            ingrediente[7].CheckBox.Enabled = estado;
            ingrediente[8].CheckBox.Enabled = estado;
            ingrediente[9].CheckBox.Enabled = estado;
            ingrediente[10].CheckBox.AutoCheck = estado;
        }
   
        private void ingredientesTodosEnabled()
        {
            for (int i = 0; i < ingrediente.Length; i++)
            {
                ingrediente[i].CheckBox.Enabled = true;
            }
        }

        public void addResumen()
        {
            listBoxResumen.Items.Clear();

            listBoxResumen.Items.Insert(0, $"Pizza: {cBEligePizza.Text}");
            listBoxResumen.Items.Insert(1, $"");
            listBoxResumen.Items.Insert(2, $"Tamaño: {cBTamanio.Text}");
            listBoxResumen.Items.Insert(3, $"Masa: {cBMasa.Text}");
            listBoxResumen.Items.Insert(4, $"");
            listBoxResumen.Items.Insert(5, $"Ingredientes:");
            for (int i = 0; i < num_ingdExtra; i++)
                listBoxResumen.Items.Insert(6 + i, $"\t{i+1}.- {nom_indgExtra[i]}");

        }

        public int calculaFactura()
        {
            int factura = 0;

            // Añade a la factura el tipo de Pizza elegida
            switch (cBEligePizza.Text)
            {
                case "New York":
                    factura += 6;
                    break;
                case "Vegetariana":
                    factura += 5;
                    break;
                case "Barbacoa Picante":
                    factura += 7;
                    break;
                case "4 Quesos":
                    factura += 10;
                    break;
            }

            // Añade a la factura el tipo de tamaño elegida
            switch (cBTamanio.Text)
            {
                case "Pequeño":
                    factura += 3;
                    break;
                case "Mediano":
                    factura += 5;
                    break;
                case "Familiar":
                    factura += 8;
                    break;
            }

            // Añade a la factura el tipo de masa elegida
            switch (cBMasa.Text)
            {
                case "Fina":
                    factura += 1;
                    break;
                case "Pan":
                    factura += 2;
                    break;
                case "Tradicional":
                    factura += 1;
                    break;
                case "Bordes Rellenos":
                    factura += 3;
                    break;
            }
            
            // Si es 4 Quesos no se añaden ingredientes extra en el importe
            if (cBEligePizza.Text != "4 Quesos")
                factura += num_ingdExtra - 2;

            return factura;
        }

        public void muestraFactura()
        {
            MessageBox.Show(
                $"Tu pedido de una pizza: {cBEligePizza.Text}\n\n" +
                $"Tamaño {cBTamanio.Text}.\n" +
                $"Masa {cBMasa.Text}.\n" +
                $"Con {num_ingdExtra} ingredientes.\n\n" +
                $"El importe de su pedido es: {calculaFactura()}€\n\n" +
                $"\t\tGracias.",
                "Factura", MessageBoxButtons.OK);
        }

        // Eventos
        private void Ingd_CheckedChanged(object sender, EventArgs e)
        {
            if (cBEligePizza.Text != "")
            {
                for (int i = 0; i < ingrediente.Length; i++)
                {
                    if (sender == ingrediente[i].CheckBox && ingrediente[i].CheckBox.AutoCheck)
                    {
                        // Cuando selecionas el objeto
                        if (ingrediente[i].CheckBox.Checked)
                        {
                            if (num_ingdExtra < 6)
                                nom_indgExtra[num_ingdExtra++] = ingrediente[i].Nombre;
                            else
                            {
                                ingrediente[i].CheckBox.Checked = false;
                                MessageBox.Show("No puedes añadir mas ingredientes.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            for (int j = 0; j < num_ingdExtra; j++)
                            {
                                if (nom_indgExtra[j] == ingrediente[i].Nombre)
                                {
                                    for (int k = j; k < num_ingdExtra - 1; k++)
                                        nom_indgExtra[k] = nom_indgExtra[k + 1];
                                    num_ingdExtra--;
                                    j = num_ingdExtra;
                                }
                            }
                        }
                    }
                }
            }

            addResumen();
        }

        private void cBTamanio_SelectedIndexChanged(object sender, EventArgs e)
        {
            addResumen();
        }

        private void cBMasa_SelectedIndexChanged(object sender, EventArgs e)
        {
            addResumen();
        }

        private void btnImporte_Click(object sender, EventArgs e)
        {
            if (cBEligePizza.Text == "4 Quesos" && cBTamanio.Text != "")
                muestraFactura();
            else if (cBEligePizza.Text != "4 Quesos" && cBEligePizza.Text != "")
            {
                if (cBTamanio.Text != "" && cBMasa.Text != "")
                {
                    if (num_ingdExtra >= 4 && num_ingdExtra <= 6)
                        muestraFactura();
                    else
                        MessageBox.Show("Tiene que haber como mínimo 4 y máximo 6 Ingredientes.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    MessageBox.Show("Debes añadir el Tamaño y la Masa.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MessageBox.Show("Debes añadir la Pizza.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);


            for (int i = 0; i < num_ingdExtra; i++)
            {
                Console.WriteLine(nom_indgExtra[i]);
            }
        }
    }
}
