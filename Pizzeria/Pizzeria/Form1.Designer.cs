﻿using System.Windows.Forms;

namespace Pizzeria
{
    partial class Formulario
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            DialogResult result = MessageBox.Show("¿Quieres salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulario));
            this.labelPredefinidas = new System.Windows.Forms.Label();
            this.cBEligePizza = new System.Windows.Forms.ComboBox();
            this.lblListaIngre = new System.Windows.Forms.Label();
            this.labelTamanio = new System.Windows.Forms.Label();
            this.cBTamanio = new System.Windows.Forms.ComboBox();
            this.labelMasa = new System.Windows.Forms.Label();
            this.cBMasa = new System.Windows.Forms.ComboBox();
            this.btnImporte = new System.Windows.Forms.Button();
            this.panelIngd = new System.Windows.Forms.Panel();
            this.Ingd0 = new System.Windows.Forms.CheckBox();
            this.Ingd1 = new System.Windows.Forms.CheckBox();
            this.Ingd2 = new System.Windows.Forms.CheckBox();
            this.Ingd3 = new System.Windows.Forms.CheckBox();
            this.Ingd4 = new System.Windows.Forms.CheckBox();
            this.Ingd5 = new System.Windows.Forms.CheckBox();
            this.Ingd6 = new System.Windows.Forms.CheckBox();
            this.Ingd7 = new System.Windows.Forms.CheckBox();
            this.Ingd8 = new System.Windows.Forms.CheckBox();
            this.Ingd9 = new System.Windows.Forms.CheckBox();
            this.Ingd10 = new System.Windows.Forms.CheckBox();
            this.Ingd11 = new System.Windows.Forms.CheckBox();
            this.Ingd12 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxResumen = new System.Windows.Forms.ListBox();
            this.pizza = new System.Windows.Forms.Label();
            this.panelIngd.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelPredefinidas
            // 
            this.labelPredefinidas.AutoSize = true;
            this.labelPredefinidas.Location = new System.Drawing.Point(95, 51);
            this.labelPredefinidas.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelPredefinidas.Name = "labelPredefinidas";
            this.labelPredefinidas.Size = new System.Drawing.Size(168, 19);
            this.labelPredefinidas.TabIndex = 1;
            this.labelPredefinidas.Text = "Pizzas Predefinidas";
            // 
            // cBEligePizza
            // 
            this.cBEligePizza.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBEligePizza.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBEligePizza.FormattingEnabled = true;
            this.cBEligePizza.Items.AddRange(new object[] {
            "New York",
            "Vegetariana",
            "Barbacoa Picante",
            "4 Quesos"});
            this.cBEligePizza.Location = new System.Drawing.Point(99, 86);
            this.cBEligePizza.Name = "cBEligePizza";
            this.cBEligePizza.Size = new System.Drawing.Size(243, 27);
            this.cBEligePizza.TabIndex = 2;
            this.cBEligePizza.SelectedIndexChanged += new System.EventHandler(this.cBEligePizza_SelectedIndexChanged);
            // 
            // lblListaIngre
            // 
            this.lblListaIngre.AutoSize = true;
            this.lblListaIngre.Location = new System.Drawing.Point(473, 51);
            this.lblListaIngre.Name = "lblListaIngre";
            this.lblListaIngre.Size = new System.Drawing.Size(182, 19);
            this.lblListaIngre.TabIndex = 4;
            this.lblListaIngre.Text = "Lista de Ingredientes";
            // 
            // labelTamanio
            // 
            this.labelTamanio.AutoSize = true;
            this.labelTamanio.Location = new System.Drawing.Point(95, 157);
            this.labelTamanio.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelTamanio.Name = "labelTamanio";
            this.labelTamanio.Size = new System.Drawing.Size(137, 19);
            this.labelTamanio.TabIndex = 1;
            this.labelTamanio.Text = "Tamaño Pizzas ";
            // 
            // cBTamanio
            // 
            this.cBTamanio.DisplayMember = "Pequeño";
            this.cBTamanio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBTamanio.Enabled = false;
            this.cBTamanio.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBTamanio.FormattingEnabled = true;
            this.cBTamanio.Items.AddRange(new object[] {
            "Pequeño",
            "Mediano",
            "Familiar"});
            this.cBTamanio.Location = new System.Drawing.Point(99, 192);
            this.cBTamanio.Name = "cBTamanio";
            this.cBTamanio.Size = new System.Drawing.Size(243, 27);
            this.cBTamanio.TabIndex = 1;
            this.cBTamanio.SelectedIndexChanged += new System.EventHandler(this.cBTamanio_SelectedIndexChanged);
            // 
            // labelMasa
            // 
            this.labelMasa.AutoSize = true;
            this.labelMasa.Location = new System.Drawing.Point(95, 248);
            this.labelMasa.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelMasa.Name = "labelMasa";
            this.labelMasa.Size = new System.Drawing.Size(108, 19);
            this.labelMasa.TabIndex = 1;
            this.labelMasa.Text = "Masa Pizzas";
            // 
            // cBMasa
            // 
            this.cBMasa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBMasa.Enabled = false;
            this.cBMasa.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBMasa.FormattingEnabled = true;
            this.cBMasa.Items.AddRange(new object[] {
            "Fina",
            "Pan",
            "Tradicional",
            "Bordes Rellenos"});
            this.cBMasa.Location = new System.Drawing.Point(99, 283);
            this.cBMasa.Name = "cBMasa";
            this.cBMasa.Size = new System.Drawing.Size(243, 27);
            this.cBMasa.TabIndex = 2;
            this.cBMasa.SelectedIndexChanged += new System.EventHandler(this.cBMasa_SelectedIndexChanged);
            // 
            // btnImporte
            // 
            this.btnImporte.Location = new System.Drawing.Point(924, 422);
            this.btnImporte.Name = "btnImporte";
            this.btnImporte.Size = new System.Drawing.Size(133, 41);
            this.btnImporte.TabIndex = 5;
            this.btnImporte.Text = "Importe";
            this.btnImporte.UseVisualStyleBackColor = true;
            this.btnImporte.Click += new System.EventHandler(this.btnImporte_Click);
            // 
            // panelIngd
            // 
            this.panelIngd.Controls.Add(this.Ingd0);
            this.panelIngd.Controls.Add(this.Ingd1);
            this.panelIngd.Controls.Add(this.Ingd2);
            this.panelIngd.Controls.Add(this.Ingd3);
            this.panelIngd.Controls.Add(this.Ingd4);
            this.panelIngd.Controls.Add(this.Ingd5);
            this.panelIngd.Controls.Add(this.Ingd6);
            this.panelIngd.Controls.Add(this.Ingd7);
            this.panelIngd.Controls.Add(this.Ingd8);
            this.panelIngd.Controls.Add(this.Ingd9);
            this.panelIngd.Controls.Add(this.Ingd10);
            this.panelIngd.Controls.Add(this.Ingd11);
            this.panelIngd.Controls.Add(this.Ingd12);
            this.panelIngd.Location = new System.Drawing.Point(477, 86);
            this.panelIngd.Name = "panelIngd";
            this.panelIngd.Size = new System.Drawing.Size(216, 377);
            this.panelIngd.TabIndex = 1;
            // 
            // Ingd0
            // 
            this.Ingd0.AutoCheck = false;
            this.Ingd0.AutoSize = true;
            this.Ingd0.Location = new System.Drawing.Point(3, 3);
            this.Ingd0.Name = "Ingd0";
            this.Ingd0.Size = new System.Drawing.Size(124, 23);
            this.Ingd0.TabIndex = 1;
            this.Ingd0.Text = "Jamon York";
            this.Ingd0.UseVisualStyleBackColor = true;
            this.Ingd0.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // Ingd1
            // 
            this.Ingd1.AutoCheck = false;
            this.Ingd1.AutoSize = true;
            this.Ingd1.Location = new System.Drawing.Point(3, 32);
            this.Ingd1.Name = "Ingd1";
            this.Ingd1.Size = new System.Drawing.Size(137, 23);
            this.Ingd1.TabIndex = 2;
            this.Ingd1.Text = "Champiñones";
            this.Ingd1.UseVisualStyleBackColor = true;
            this.Ingd1.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // Ingd2
            // 
            this.Ingd2.AutoCheck = false;
            this.Ingd2.AutoSize = true;
            this.Ingd2.Location = new System.Drawing.Point(3, 61);
            this.Ingd2.Name = "Ingd2";
            this.Ingd2.Size = new System.Drawing.Size(107, 23);
            this.Ingd2.TabIndex = 2;
            this.Ingd2.Text = "Calabacín";
            this.Ingd2.UseVisualStyleBackColor = true;
            this.Ingd2.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // Ingd3
            // 
            this.Ingd3.AutoCheck = false;
            this.Ingd3.AutoSize = true;
            this.Ingd3.Location = new System.Drawing.Point(3, 90);
            this.Ingd3.Name = "Ingd3";
            this.Ingd3.Size = new System.Drawing.Size(108, 23);
            this.Ingd3.TabIndex = 1;
            this.Ingd3.Text = "Aceitunas";
            this.Ingd3.UseVisualStyleBackColor = true;
            this.Ingd3.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // Ingd4
            // 
            this.Ingd4.AutoCheck = false;
            this.Ingd4.AutoSize = true;
            this.Ingd4.Location = new System.Drawing.Point(3, 119);
            this.Ingd4.Name = "Ingd4";
            this.Ingd4.Size = new System.Drawing.Size(95, 23);
            this.Ingd4.TabIndex = 2;
            this.Ingd4.Text = "Tabasco";
            this.Ingd4.UseVisualStyleBackColor = true;
            this.Ingd4.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // Ingd5
            // 
            this.Ingd5.AutoCheck = false;
            this.Ingd5.AutoSize = true;
            this.Ingd5.Location = new System.Drawing.Point(3, 148);
            this.Ingd5.Name = "Ingd5";
            this.Ingd5.Size = new System.Drawing.Size(93, 23);
            this.Ingd5.TabIndex = 3;
            this.Ingd5.Text = "Ternera";
            this.Ingd5.UseVisualStyleBackColor = true;
            this.Ingd5.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // Ingd6
            // 
            this.Ingd6.AutoCheck = false;
            this.Ingd6.AutoSize = true;
            this.Ingd6.Location = new System.Drawing.Point(3, 177);
            this.Ingd6.Name = "Ingd6";
            this.Ingd6.Size = new System.Drawing.Size(119, 23);
            this.Ingd6.TabIndex = 1;
            this.Ingd6.Text = "Queso Azul";
            this.Ingd6.UseVisualStyleBackColor = true;
            this.Ingd6.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // Ingd7
            // 
            this.Ingd7.AutoCheck = false;
            this.Ingd7.AutoSize = true;
            this.Ingd7.Location = new System.Drawing.Point(3, 206);
            this.Ingd7.Name = "Ingd7";
            this.Ingd7.Size = new System.Drawing.Size(174, 23);
            this.Ingd7.TabIndex = 2;
            this.Ingd7.Text = "Queso Parmesano";
            this.Ingd7.UseVisualStyleBackColor = true;
            this.Ingd7.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // Ingd8
            // 
            this.Ingd8.AutoCheck = false;
            this.Ingd8.AutoSize = true;
            this.Ingd8.Location = new System.Drawing.Point(3, 235);
            this.Ingd8.Name = "Ingd8";
            this.Ingd8.Size = new System.Drawing.Size(132, 23);
            this.Ingd8.TabIndex = 2;
            this.Ingd8.Text = "Queso Suave";
            this.Ingd8.UseVisualStyleBackColor = true;
            this.Ingd8.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // Ingd9
            // 
            this.Ingd9.AutoCheck = false;
            this.Ingd9.AutoSize = true;
            this.Ingd9.Location = new System.Drawing.Point(3, 264);
            this.Ingd9.Name = "Ingd9";
            this.Ingd9.Size = new System.Drawing.Size(179, 23);
            this.Ingd9.TabIndex = 2;
            this.Ingd9.Text = "Queso Mozzarrella";
            this.Ingd9.UseVisualStyleBackColor = true;
            this.Ingd9.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // Ingd10
            // 
            this.Ingd10.AutoCheck = false;
            this.Ingd10.AutoSize = true;
            this.Ingd10.Location = new System.Drawing.Point(3, 293);
            this.Ingd10.Name = "Ingd10";
            this.Ingd10.Size = new System.Drawing.Size(64, 23);
            this.Ingd10.TabIndex = 1;
            this.Ingd10.Text = "Piña";
            this.Ingd10.UseVisualStyleBackColor = true;
            this.Ingd10.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // Ingd11
            // 
            this.Ingd11.AutoCheck = false;
            this.Ingd11.AutoSize = true;
            this.Ingd11.Location = new System.Drawing.Point(3, 322);
            this.Ingd11.Name = "Ingd11";
            this.Ingd11.Size = new System.Drawing.Size(101, 23);
            this.Ingd11.TabIndex = 2;
            this.Ingd11.Text = "Peperoni";
            this.Ingd11.UseVisualStyleBackColor = true;
            this.Ingd11.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // Ingd12
            // 
            this.Ingd12.AutoCheck = false;
            this.Ingd12.AutoSize = true;
            this.Ingd12.Location = new System.Drawing.Point(3, 351);
            this.Ingd12.Name = "Ingd12";
            this.Ingd12.Size = new System.Drawing.Size(77, 23);
            this.Ingd12.TabIndex = 2;
            this.Ingd12.Text = "Bacon";
            this.Ingd12.UseVisualStyleBackColor = true;
            this.Ingd12.Click += new System.EventHandler(this.Ingd_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(766, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 19);
            this.label1.TabIndex = 4;
            this.label1.Text = "Resumen:";
            // 
            // listBoxResumen
            // 
            this.listBoxResumen.BackColor = System.Drawing.Color.Tomato;
            this.listBoxResumen.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBoxResumen.FormattingEnabled = true;
            this.listBoxResumen.ItemHeight = 19;
            this.listBoxResumen.Items.AddRange(new object[] {
            "Pizza:",
            "",
            "Tamaño:",
            "Masa:",
            "",
            "Ingredientes:"});
            this.listBoxResumen.Location = new System.Drawing.Point(770, 86);
            this.listBoxResumen.Name = "listBoxResumen";
            this.listBoxResumen.Size = new System.Drawing.Size(287, 323);
            this.listBoxResumen.TabIndex = 7;
            // 
            // pizza
            // 
            this.pizza.AutoSize = true;
            this.pizza.Image = ((System.Drawing.Image)(resources.GetObject("pizza.Image")));
            this.pizza.Location = new System.Drawing.Point(102, 338);
            this.pizza.MinimumSize = new System.Drawing.Size(240, 200);
            this.pizza.Name = "pizza";
            this.pizza.Size = new System.Drawing.Size(240, 200);
            this.pizza.TabIndex = 8;
            // 
            // Formulario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tomato;
            this.ClientSize = new System.Drawing.Size(1084, 561);
            this.Controls.Add(this.pizza);
            this.Controls.Add(this.listBoxResumen);
            this.Controls.Add(this.panelIngd);
            this.Controls.Add(this.btnImporte);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblListaIngre);
            this.Controls.Add(this.cBMasa);
            this.Controls.Add(this.labelMasa);
            this.Controls.Add(this.cBTamanio);
            this.Controls.Add(this.labelTamanio);
            this.Controls.Add(this.cBEligePizza);
            this.Controls.Add(this.labelPredefinidas);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Formulario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PIZZERIA";
            this.panelIngd.ResumeLayout(false);
            this.panelIngd.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelPredefinidas;
        private System.Windows.Forms.ComboBox cBEligePizza;
        private System.Windows.Forms.Label lblListaIngre;
        private System.Windows.Forms.Label labelTamanio;
        private System.Windows.Forms.ComboBox cBTamanio;
        private System.Windows.Forms.Label labelMasa;
        private System.Windows.Forms.ComboBox cBMasa;
        private System.Windows.Forms.Button btnImporte;
        private System.Windows.Forms.Panel panelIngd;
        private System.Windows.Forms.CheckBox Ingd12;
        private System.Windows.Forms.CheckBox Ingd7;
        private System.Windows.Forms.CheckBox Ingd11;
        private System.Windows.Forms.CheckBox Ingd4;
        private System.Windows.Forms.CheckBox Ingd9;
        private System.Windows.Forms.CheckBox Ingd2;
        private System.Windows.Forms.CheckBox Ingd3;
        private System.Windows.Forms.CheckBox Ingd10;
        private System.Windows.Forms.CheckBox Ingd1;
        private System.Windows.Forms.CheckBox Ingd0;
        private System.Windows.Forms.CheckBox Ingd8;
        private System.Windows.Forms.CheckBox Ingd6;
        private System.Windows.Forms.CheckBox Ingd5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBoxResumen;
        private System.Windows.Forms.Label pizza;
    }
}

