﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzeria
{
    class Ingrediente
    {
        CheckBox checkBox;
        string nombre;

        public Ingrediente()
        {
            this.checkBox = null;
            this.nombre = "";
        }

        public Ingrediente(CheckBox ingrediente, string nombre)
        {
            this.checkBox = ingrediente;
            this.nombre = nombre;
        }

        public CheckBox CheckBox { get { return checkBox; } set { checkBox = value; } }
        public string Nombre {
            get {
                return nombre;
            }
            set {
                nombre = value;
            }
        }

        public void resetear()
        {
            CheckBox.Checked = false;
            CheckBox.AutoCheck = true;
        }

        public void fijarIngrediente()
        {
            CheckBox.Checked = true;
            CheckBox.AutoCheck = false;

            Nombre = CheckBox.Text;
        }

        public void cambiaEnabled (bool estado)
        {
            CheckBox.Checked = estado;
            CheckBox.AutoCheck = estado;
        }
    }
}
