﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03___Ejercicio1Generales
{
    class Ejercicio2
    {
        static void Main(string[] args)
        {
            double numD;
            int numI;
            double numD2;
            int op;

            numI = 0;
            numD = 764.783;
            numD2 = 0;
            op = 0;

            Console.WriteLine("El numero inicial es: " + numD);

            do
            {
                Console.Write("Elije una opcion: ");
                op = Int32.Parse(Console.ReadLine());

                switch (op)
                {
                    case 0:
                        numD = 764.783;
                        Console.WriteLine($"{numD}");
                        Console.WriteLine($"{numI}");
                        Console.WriteLine($"{numD2}");
                        break;
                    case 1:
                        Console.WriteLine($"a. { truncar(numD) }");
                        break;
                    case 2:
                        truncar(numD, ref numI);
                        Console.WriteLine($"b. { numI }");
                        break;
                    case 3:
                        numI = truncar(numD);
                        Console.WriteLine($"c. { numI }");
                        break;
                    case 4:
                        Console.WriteLine($"d. { truncar(numD, 2) }");
                        break;
                    case 5:
                        truncar(numD, 2, ref numD2);
                        Console.WriteLine($"e. { numD2 }");
                        break;
                    case 6:
                        truncar(ref numD, 2);
                        Console.WriteLine($"f. { numD }");
                        break;
                    case 7:
                        Console.WriteLine("Adios.");
                        break;

                }
            } while (op != 7);
        }

        private static int truncar(double d)
        {
            return Convert.ToInt32(Math.Truncate(d));
        }

        private static void truncar(double d, ref int i)
        {
            i = Convert.ToInt32(Math.Truncate(d));
        }

        private static double truncar(double d, int i)
        {
            return Math.Truncate(d * Math.Pow(10, i)) / Math.Pow(10, i);
        }

        private static void truncar(double d, int i, ref double num)
        {
            num = Math.Truncate(d * Math.Pow(10, i)) / Math.Pow(10, i);

        }
        private static void truncar(ref double d, int i)
        {
            d = Math.Truncate(d * Math.Pow(10, i)) / Math.Pow(10, i);
        }
    }
}
