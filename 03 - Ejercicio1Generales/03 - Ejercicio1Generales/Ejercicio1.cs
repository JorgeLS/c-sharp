﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03___Ejercicio1Generales
{
    class Ejercicio1
    {
        static void Main(string[] args)
        {
            string cadena;
            cadena = null;
            int op;

            do
            {
                Console.Write("Elije una opcion: ");
                op = Int32.Parse(Console.ReadLine());

                switch (op)
                {
                    case 0:
                        cadena = "";
                        break;
                    case 1:
                        cadena = Console.ReadLine();
                        break;
                    case 2:
                        Console.WriteLine($"Cadena = {cadena}");
                        break;
                    case 3:
                        cadena = invertir(cadena);
                        break;
                    case 4:
                        cadena = mayus(cadena);
                        break;
                    case 5:
                        cadena = min(cadena);
                        break;
                    case 6:
                        cadena = rotarDerecha(cadena);
                        break;
                    case 7:
                        cadena = rotarIzquierda(cadena);
                        break;
                    case 8:
                        Console.WriteLine("Adios.");
                        break;

                }
            } while (op != 8);
        }

        private static string invertir (string cad)
        {
            char[] buffer = cad.ToCharArray();
            char[] s = new char[buffer.Length];

            for (int i = 0; i < buffer.Length; i++)
                s[(buffer.Length-1) - i] = buffer[i];

            return new string(s);
        }

        private static string mayus(string cad)
        {
            char[] s = cad.ToCharArray();

            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] >= 'a' && s[i] <= 'z')
                    s[i] = (char)(s[i] - ('a' - 'A'));
            }
            return new string(s);
        }

        private static string min(string cad)
        {
            char[] s = cad.ToCharArray();

            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] >= 'A' && s[i] <= 'Z')
                    s[i] = (char)(s[i] + ('a' - 'A'));
            }
            return new string(s);
        }

        private static string rotarDerecha(string cad)
        {
            char[] buffer = cad.ToCharArray();
            char[] s = new char [buffer.Length];

            for (int i = 0; i < buffer.Length; i++)
                s[(i + 1) % buffer.Length] = buffer[i];

            return new string(s);
        }

        private static string rotarIzquierda(string cad)
        {
            char[] buffer = cad.ToCharArray();
            char[] s = new char[buffer.Length];

            for (int i = 0; i < buffer.Length; i++)
                    s[(i + buffer.Length - 1) % buffer.Length] = buffer[i];

            return new string(s);
        }
    }
}
